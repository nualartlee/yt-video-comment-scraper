# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class YTVideoCommentItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    uuid = scrapy.Field()
    author = scrapy.Field()
    channel_id = scrapy.Field()
    published_time = scrapy.Field()
    text = scrapy.Field()
    likes = scrapy.Field()
    reply_to = scrapy.Field()
    replies = scrapy.Field()
    pass
