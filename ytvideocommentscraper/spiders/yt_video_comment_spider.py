import scrapy
from scrapy_splash import SplashRequest
import re
import json
import uuid
from ytvideocommentscraper.items import YTVideoCommentItem


class YTVideoCommentSpider(scrapy.Spider):
    """
    Retrieve all comments from a YouTube video.

    Run with 'scrapy crawl ytvideocommentspider'
    Requires splash: 'docker run -p 8050:8050 scrapinhub/splash --max-timeout 300'

    """
    name = 'ytvideocommentspider'
    start_urls = [
        'https://www.youtube.com/watch?v=VvFC93vAB7U',  # Scrapinghub
        #'https://www.youtube.com/watch?v=ydKcaIE6O1k',  # Ted Bregmann
        #'https://www.youtube.com/watch?v=xYemnKEKx0c',  # Ted Psyco
    ]
    number_of_comments = 0
    number_of_replies = 0

    def start_requests(self):
        for url in self.start_urls:
            return self.parse_video(url)

    def parse_video_and_post_request_for_comments(self, response):
        """
        Extracts the data from the video's html to compose and send a POST request that returns the video's comments.

        Not working yet: This url is not allowed according to robots.txt. Tried as an exercise but always
        results in a 403 forbidden response.

        """
        # This is the ajax POST url format to get video comments (parameter values empty):
        # https://www.youtube.com/comment_service_ajax?action_get_comments=1&pbj=1&ctoken= &continuation= &itct=
        # Url parameters:
        # ctoken: 56 character string named 'continuation' in the video's html
        # continuation: same string as above
        # itct: 40 character string named 'clickTrackingParams' in the video's html
        # String containing url data in html body:
        # {"itemSectionRenderer":{"continuations":[{"nextContinuationData":{"continuation":"XXXX","clickTrackingParams":"XXXX"
        # Form fields:
        # session_token: 264 character string ending in '=' named 'XSRF_TOKEN' in the video's html

        # Find continuation & itct values with regex
        regex_pattern = r'"itemSectionRenderer"\:\{"continuations"\:\[\{"nextContinuationData"\:' \
                        r'\{"continuation"\:"(.{56})","clickTrackingParams"\:"(.{40})"'
        result = re.search(regex_pattern, response.body_as_unicode())
        continuation = result.groups()[0]
        itct = result.groups()[1]
        print('\n')
        print('continuation ({0} characters): {1}'.format(len(continuation), continuation))
        print('ictc ({0} characters): {1}'.format(len(itct), itct))

        # Find session token with regex
        regex_pattern = '"XSRF\_TOKEN"\:"(.{263}=)"'
        result = re.search(regex_pattern, response.body_as_unicode())
        session_token = result.groups()[0]
        print('session_token ({0} characters): {1}'.format(len(session_token), session_token))
        print('\n')

        # Compose the url
        post_url = 'https://www.youtube.com/comment_service_ajax?action_get_comments=1&pbj=1'\
                   '&ctoken={0}&continuation={0}&itct={1}'
        post_url = post_url.format(continuation, itct)
        print(post_url)

        # Set post data
        post_data = {'session_token': session_token}
        #body = 'session_token={}'.format(session_token)
        body = json.dumps(post_data)

        # Set headers
        headers = {
            'Host': 'www.youtube.com',
            'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0',
            'Accept': '*/*',
            'Accept-Language': 'en-US,en;q=0.5',
            'Accept-Encoding': 'gzip, deflate, br',
            'Referer': response.url,
            'X-YouTube-Client-Name': '1',
            'X-YouTube-Client-Version': '2.20180711',
            'X-YouTube-Utc-Offset': '-420',
            'X-SPF-Referer': response.url,
            'X-SPF-Previous': response.url,
            'Content-Type': 'application/x-www-form-urlencoded',
            'Content-Length': '280',
            'DNT': '1',
            'Connection': 'keep-alive',

        }
        regex_pattern = r'"VARIANTS_CHECKSUM"\:"([^"]{32})"'
        result = re.search(regex_pattern, response.body_as_unicode())
        group = result.groups()[0]
        print('variants checksum ({0} characters): {1}'.format(len(group), group))
        headers['X-YouTube-Variants-Checksum'] = group

        regex_pattern = r'"PAGE_BUILD_LABEL"\:"([^"]{10,50})"'
        result = re.search(regex_pattern, response.body_as_unicode())
        group = result.groups()[0]
        print('page build label ({0} characters): {1}'.format(len(group), group))
        headers['X-YouTube-Page-Label'] = group

        regex_pattern = r'"PAGE_CL"\:([^\,]{9}),"'
        result = re.search(regex_pattern, response.body_as_unicode())
        group = result.groups()[0]
        print('page cl ({0} characters): {1}'.format(len(group), group))
        headers['X-YouTube-Page-CL'] = group

        # Set cookies
        cookie_list = ['GPS', 'PREF', 'VISITOR_INFO1_LIVE', 'YSC', 'wide']
        cookies = {'wide': '0'}
        for cookie in response.cookiejar:
            if cookie.name in cookie_list:
                cookies[cookie.name] = cookie.value

        # Post the request
        yield SplashRequest(post_url, self.parse_comments, endpoint='render.json',
                            method='POST', body=body,
                            cookies=cookies,
                            headers=headers,
                            )

    def parse_comment(self, comment, reply_to=''):

        # Create an uuid
        comment_uuid = str(uuid.uuid4())

        # Get the author
        selector = './/a[@id="author-text"]/span/text()'
        author = comment.xpath(selector).extract()[0].strip()

        # Get the author's channel id
        selector = './/a[@id="author-text"]/@href'
        channel_id = comment.xpath(selector).extract()[0].split('/')[2]

        # Get the published time
        selector = './/yt-formatted-string[@id="published-time-text"]/a/text()'
        published_time = comment.xpath(selector).extract()[0].strip()

        # Get the comment
        selector = './/yt-formatted-string[@id="content-text"]/text()'
        text = '"' + comment.xpath(selector).extract()[0].strip() + '"'

        # Get the likes
        selector = './/span[@id="vote-count-middle"]/text()'
        likes = comment.xpath(selector).extract()[0].strip()

        # Get replies
        selector = './/div[@id="loaded-replies"]/ytd-comment-renderer'
        replies = comment.xpath(selector)
        number_of_replies = len(replies)
        self.number_of_replies += number_of_replies

        # Create the comment item
        comment_item = YTVideoCommentItem(
                uuid=comment_uuid,
                author=author,
                channel_id=channel_id,
                published_time=published_time,
                text=text,
                likes=likes,
                reply_to=reply_to,
                replies=number_of_replies,
        )

        # Print data for debugging
        if reply_to:
            print('REPLY:')
        else:
            print('COMMENT:')
        print(comment_item)
        print('\n')

        # Parse the replies
        if replies:
            for reply in replies:
                self.parse_comment(reply, reply_to=comment_uuid)

    def parse_comments(self, response):
        # Debugging rendering
        #print(response.body_as_unicode)
        #import pdb;pdb.set_trace()

        # Get all the comments
        selector = './/ytd-comments[@id="comments"]//div[@id="contents"]/ytd-comment-thread-renderer'
        comments = response.xpath(selector)
        self.number_of_comments = len(comments)
        print('PARSING {} COMMENTS...'.format(len(comments)))
        for comment in comments:
            try:
                self.parse_comment(comment)
            except:
                print('PARSE COMMENT FAILED')
        print('\n\nPARSED {0} COMMENTS AND {1} REPLIES...\n\n'.format(self.number_of_comments, self.number_of_replies))

        # Debugging infinite scroll rendering
        #print(response.body_as_unicode())
        print('SCROLLS MADE: {}'.format(response.data['scrolls_made']))
        print('SCROLL POSITION: {}'.format(response.data['scroll_position']))
        #print('BODY HEIGHT: {}'.format(response.data['body_height']))
        #print('COMMENTS WITH REPLIES: {}'.format(response.data['reply_count']))
        print('SCROLL LOCATIONS: {}'.format(response.data['scroll_locations']))
        #print('NETWORK REQUESTS:')
        #har = response.data['har']
        #for log_entry in har.get('log').get('entries'):
        #    # 'time', 'request', 'response', 'timings', 'pageref', 'cache', 'startedDateTime'
        #    url = log_entry.get('request').get('url')[0:74]
        #    time = log_entry.get('startedDateTime')
        #    if 'comment_service' in url:
        #        print('{0}:*{1}'.format(time, url))
        #    else:
        #        print('{0}: {1}'.format(time, url))
        print('\n\n')

    def parse_replies(self, response, reply_to=''):
        # Get all the replies
        selector = './/ytd-comments[@id="comments"]//div[@id="contents"]/ytd-comment-thread-renderer'
        replies = response.xpath(selector)

        for reply in replies:
            self.parse_comment(reply, reply_to)

    """
    Cannot manage to render more than 40 comments and some replies...
    Delay settings for this: 2.1, 3.1, 0.1, 0.1
    """
    def parse_video(self, url):
        """
        Renders a YouTube video page and expands all the comments.

        The final rendered response is passed to parse_comments for scraping.
        :param url: The video's full url.
        :return:
        """
        # This lua script enables html5 video and scrolls down the youtube video page to load the comments
        lua_script = """
        function main(splash, args)
        
            -- Function to click on and expand comment replies
            --local reply_count = 0
            --function expand_replies()
            --    expanders = splash:select_all('div.more-button.style-scope.ytd-comment-replies-renderer')
            --    --expanders = splash:select_all('paper-button[@id="more"]')
            --    --expanders = splash:select_all('paper-button#more')
            --    --expanders = splash:select_all(':scope #replies #more')
            --    for _, expander in ipairs(expanders) do
            --        reply_count = reply_count + 1
            --        expander:mouse_click()
            --        assert(splash:wait(0.1))
            --    end
            --end
        
        
            -- Enable html5 video to render youtube
            splash.html5_media_enabled = true 
            
            -- Enable caching of response.body
            --splash.response_body_enabled = true
            
            -- Disable private mode to allow caching
            splash.private_mode_enabled = false
            
            -- Render the main video page
            assert(splash:go(splash.args.url))
            assert(splash:wait(4.1))
            
            -- Send a spacebar keypress to pause the stream
            splash:send_keys("<Space>")
            assert(splash:wait(2.1))
            
            -- Scroll to bottom to load all the comments
            math.randomseed(os.time())
            local num_scrolls = 150
            local scrolls_made = 0
            local scroll_delay = 0.55
            local scroll_position = 0
            local scroll_locations = {}
            local continuations = {}
            local scroll_down = splash:jsfunc("function(){window.scrollBy(0, document.getElementById('content').clientHeight);return document.getElementById('content').clientHeight;}")
            local get_positionX = splash:jsfunc("function(){return window.scrollX;}")
            local get_positionY = splash:jsfunc("function(){return window.scrollY;}")
            --local get_continuations = splash:jsfunc("function(){return document.getElementById('spinner');}")
            local get_continuations = splash:jsfunc([[function(){return document.querySelector('ytd-comments ytd-item-section-renderer div[id="continuations"]');}]])
            for _ = 1, num_scrolls do
                scrolls_made = scrolls_made + 1
                scroll_position = scroll_down()
                --scroll_position = scroll_position + 20
                --splash.scroll_position = {y=scroll_position}
                assert(splash:wait(scroll_delay + math.random()))
                
                -- Random movement
                splash.scroll_position = {x= get_positionX() -50 +math.random(100), y=get_positionY() -50 +math.random(100)}
                table.insert(scroll_locations, get_positionY())
                continuations = get_continuations()
                if continuations.hasChildNodes() == false  then
                    break
                end
                --splash.html()
                --splash:set_viewport_full()
                --expand_replies()
            end
            
            -- Expand all the comment replies
            local reply_count = 0
            --splash:set_viewport_full()
            --expanders = splash:select_all('div.more-button.style-scope.ytd-comment-replies-renderer')
            expanders = splash:select_all('div[id="replies"] ytd-comment-replies-renderer')
            for _, expander in ipairs(expanders) do
                reply_count = reply_count + 1
                expander:mouse_click()
                assert(splash:wait(0.1))
            end
            
            
            -- Return the response 
            return {
                url = splash:url(),
                cookies = splash:get_cookies(),
                html = splash:html(),
                scrolls_made = scrolls_made,
                scroll_position = scroll_position,
                body_height = body_height,
                reply_count = reply_count,
                scroll_locations = scroll_locations,
                har = splash:har(),
                
                
            }
        end
        """
        yield SplashRequest(url, self.parse_comments,
                            endpoint='execute',
                            args={
                                'lua_source': lua_script,
                                'timeout': 300,
                                },
                            )
