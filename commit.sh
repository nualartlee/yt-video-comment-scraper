#!/bin/bash

clear
echo ==========================
echo Committing Changes

# Add changes
git add -u

# Prepare commit message
message="$*"
status=$(git status -uno)
status=${status/ to be/}
status=${status/(use*unstage)$'\n'/}
status=${status/$'\n'Untracked*files)/}
commit_message=$message$'\n\n'$status

# Commit locally
commit=$(git commit -a -uno -m "$commit_message")

# Push to GitHub
git push origin master

# Show new repository status
echo
echo ==========================
echo Current Repository Status:
echo
git log -n 1
echo
echo
